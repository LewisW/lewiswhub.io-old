---
published: false
---

## Semantic versioning and continuous deployment

It's probably mostly thanks to the continued adoption of Composer, but I've noticed there's great increase in the number of projects that are starting to use semantic versioning in their own applications.

However, [semantic version and continious deployment don't mix well](http://blog.ploeh.dk/2013/12/10/semantic-versioning-with-continuous-deployment/). The problem is that traditionally most build servers will be responsible for versioning the deployment, and they tend to prefer easily generatable numbers (so usually automatically incremented, or date-based numbers).

However, if you use git tags to handle your versioning of your code, there's a nifty little trick your build server can use to work out the current version of your code:

```
 git describe --abbrev=0 --tags
```

This will output the latest tag in your branch, giving your build server something to work from. There are a couple of gotchas to watch out for though.

### Gotchas
The first thing to be careful for, is that this command will error if no tags were found in your branch. This one is quite easy to prepare for though, we can simply but in a default:

```
git describe --abbrev=0 --tags 2> /dev/null || echo 'v0.0.1'
```

The second gotcha is that this will only pull the latest tag - meaning it's not guaranteed to be unique for each commit. So if you've committed without incrementing the tag, your build server will potentially redeploy a different commit as the same version. This may or may not be desirable, for example if your deployment/provision process is quite lengthy and you want the ability to do quick hotfixes, then this would save your build server redeploying an entire new server and/or version. Thankfully, git can provide us with a guaranteed unique tag with the following:

```
git describe --tags 2> /dev/null || echo 'g'`git rev-parse --short HEAD`
```

This will return a version in the form of v1.2.3-44-ge5d4e, or just ge5d4e if there are no tags yet. [More information about what this command returns can be found here](http://wygoda.net/blog/getting-useful-git-revision-information)
