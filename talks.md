---
layout: page
title: Talks
permalink: /talks/
---

### Talks and presentations

Whilst I consider myself a terrible presenter, apparently I love publicly ranting enough to just about overcome [impostor syndrome](http://en.wikipedia.org/wiki/Impostor_syndrome). Here you will find slides from a number of my presentations. 

**Disclaimer:** I can only preach my experiences, as-such these are constantly changing - so if my presentations contradict previous ones, that's because my experiences have changed! Likewise, your experiences are likely to vary.

- Radid Application Development using Frameworks - De Montfort University, 03/03/14.
- [Paying off technical debt with PHPSpec](http://www.slideshare.net/LewisWright1/paying-off-technical-debt-with-phpspec) - [PHPEM](http://phpem.info/11-may-1st-2014), 01/05/14.
- [RAD != **R**ushed **A**wful **D**ecisions](http://www.slideshare.net/LewisWright1/rad-40646267) - [PHPNW conference](http://joind.in/talk/view/12149), 04/10/14.

_Actual slides coming soon!_
