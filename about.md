---
layout: page
title: About
permalink: /about/
---

### About me

I've been programming for over a decade, starting just in time to enjoy the millennium bug fiasco. I started off as a frontend developer, but soon progressed to learn everyone's favourite language... PHP. Since then I've learnt and enjoy many other languages, including Python, Javascript, C, Java, and to some extend Haskell. Whilst Python is my favourite language in terms of my sanity, the PHP community - particularly the Symfony community - has won my heart, and is my primary language.

I also own and run a small software development company, called Viva IT. Based in the heart of the UK (that's Leicester, by the way), we specialise in utilising technology to help businesses improve their efficiency. Most of the time, this invovles creating highly specialised backend systems (usually built in Symfony) to compliment and aid their business processes. Which is where my real passion lies - building high quality software that helps people do their jobs better.

### Contact me

If you want to get in touch with me, you can find me on twitter: @allwrightythen
